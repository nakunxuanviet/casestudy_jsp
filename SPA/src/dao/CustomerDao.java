package dao;

import model.CustomerModel;
import utils.DBConnection;

import java.sql.*;

public class CustomerDao {
    public boolean createkhachhang(CustomerModel customer) throws SQLException,ClassNotFoundException {
        String sql = "INSERT INTO customer (id_customer,name_customer,phone) VALUES (?,?,?,?)";

        //L?y chu?i k?t n?i t?i CSDL truy?n vào bi?n conn
        DBConnection db = new DBConnection();
        Connection conn = db.getConnection();

        PreparedStatement statement = null;
        try {
            statement = conn.prepareStatement(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        statement.setInt(1, customer.getId_customer());
        statement.setString(2, customer.getName_customer());
        statement.setInt(3, customer.getPhone());

        boolean rowInserted = statement.executeUpdate() > 0;
        statement.close();

        // region Gi?i phóng tài nguyên -- dành cho stmt
        try {
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return rowInserted;
    }

    public int getidkhachhang() throws SQLException {
        //Lấy chuỗi kết nối tới CSDL truyền vào biến conn
        DBConnection db = new DBConnection();
        Connection conn = db.getConnection();

        //Tạo đường dẫn kết nối tới CSDL
        Statement statement = null;
        try {
            statement = conn.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String sql = "SELECT id_customer,name_customer,phone FROM customer ORDER BY id DESC LIMIT 1";

        ResultSet resultSet = statement.executeQuery(sql);
        int id = 0;
        if (resultSet.next()) {

            id = resultSet.getInt("id");
        }
        resultSet.close();
        statement.close();

        return id;
    }
}
