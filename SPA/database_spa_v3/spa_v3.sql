-- MySQL dump 10.17  Distrib 10.3.16-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: spa_v0
-- ------------------------------------------------------
-- Server version	10.3.16-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id_categories` int(11) NOT NULL AUTO_INCREMENT,
  `name_categories` text DEFAULT NULL,
  PRIMARY KEY (`id_categories`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (111,'Hair'),(112,'Skin'),(113,'Jewelry'),(114,'Nail Paints');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id_customer` int(11) NOT NULL AUTO_INCREMENT,
  `name_customer` varchar(50) DEFAULT NULL,
  `phone` float DEFAULT NULL,
  PRIMARY KEY (`id_customer`)
) ENGINE=InnoDB AUTO_INCREMENT=2260 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (2256,'Dương Thị Châu',934123000),(2257,'Nguyễn Thị Nhật Linh',905060000),(2258,'Dương Mỹ Linh',988089000),(2259,'Mai Phương Thúy',988881000);
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `id_employee` int(11) NOT NULL AUTO_INCREMENT,
  `name_employee` varchar(50) DEFAULT NULL,
  `sex_employee` tinyint(1) DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_employee`)
) ENGINE=InnoDB AUTO_INCREMENT=1005 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1001,'Nguyễn Văn An',1,935456345),(1002,'Dương Thị Hồng',0,935456446),(1003,'Nguyễn Thị Bé',0,912345123),(1004,'Cao Thị Thảo',0,905060712);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id_orders` int(11) NOT NULL AUTO_INCREMENT,
  `order_date` date DEFAULT NULL,
  `id_employee` int(11) DEFAULT NULL,
  `id_customer` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_orders`),
  KEY `id_employee` (`id_employee`),
  KEY `id_customer` (`id_customer`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id_employee`),
  CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id_customer`)
) ENGINE=InnoDB AUTO_INCREMENT=325 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (321,'2019-06-20',1001,2256),(322,'2019-06-20',1002,2257),(323,'2019-06-22',1003,2258),(324,'2019-06-23',1004,2259);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordersdetails`
--

DROP TABLE IF EXISTS `ordersdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordersdetails` (
  `id_orders` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `amount` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `total_money` float DEFAULT NULL,
  PRIMARY KEY (`id_orders`,`id_product`),
  KEY `id_product` (`id_product`),
  CONSTRAINT `ordersdetails_ibfk_1` FOREIGN KEY (`id_orders`) REFERENCES `orders` (`id_orders`),
  CONSTRAINT `ordersdetails_ibfk_2` FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordersdetails`
--

LOCK TABLES `ordersdetails` WRITE;
/*!40000 ALTER TABLE `ordersdetails` DISABLE KEYS */;
INSERT INTO `ordersdetails` VALUES (323,4563,1,450000,450000);
/*!40000 ALTER TABLE `ordersdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id_product` int(11) NOT NULL AUTO_INCREMENT,
  `name_product` varchar(255) DEFAULT NULL,
  `original_price` float DEFAULT NULL,
  `price` float DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`id_product`),
  KEY `id_categories` (`category_id`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id_categories`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4569 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (100,'Alpha & Cetaphil',252,238,300,112,'/template/user/images/Skin1.png','Lorem ipsum dolor sit amet, consectetur adipiscing elit'),(101,'Relove   Moisturizer',22,25,30,112,'/template/user/images/Skin4.png','Donec sit amet auctor sem. Quisque condimentum ullamcorper bibendum.'),(102,' Moisturizer',22,25,30,112,'/template/user/images/Skin5.png','Donec sit amet auctor sem. Quisque condimentum ullamcorper bibendum.'),(108,'Dolce & Gabbana',22,25,30,112,'/template/user/images/Skin2.png','Donec sit amet auctor sem. Quisque condimentum ullamcorper bibendum.'),(109,'Dolce & Gabbana ',22,25,30,113,'/template/user/images/JEW1.png','Donec sit amet auctor sem. Quisque condimentum ullamcorper bibendum.'),(113,'Dolce & Gabbana',22,25,30,113,'/template/user/images/JEW3.png','Donec sit amet auctor sem. Quisque condimentum ullamcorper bibendum.'),(114,'Gabbana Perfume',22,25,30,113,'/template/user/images/JEW4.png','Donec sit amet auctor sem. Quisque condimentum ullamcorper bibendum.'),(116,'Perfume',22,25,30,113,'/template/user/images/JEW3.png','Donec sit amet auctor sem. Quisque condimentum ullamcorper bibendum.'),(119,'Gabbana Perfume',22,25,30,113,'/template/user/images/JEW2.png','Donec sit amet auctor sem. Quisque condimentum ullamcorper bibendum.'),(120,'Christian Louboutin',20,33,40,114,'/template/user/images/nail_paint1.jpg','Mauris hendrerit commodo justo, sit amet tempor odio commodo id.'),(121,'Christian Louboutin ',20,33,40,114,'/template/user/images/nail_paint2.jpg','Mauris hendrerit commodo justo, sit amet tempor odio commodo id.'),(131,'Louboutin Silk',20,33,40,111,'/template/user/images/hair1.jpg','Mauris hendrerit commodo justo, sit amet tempor odio commodo id.'),(132,'Louboutin',20,33,40,111,'/template/user/images/hair2.jpg','Mauris hendrerit commodo justo, sit amet tempor odio commodo id.'),(133,'Silk',20,33,40,111,'/template/user/images/hair3.jpg','Mauris hendrerit commodo justo, sit amet tempor odio commodo id.'),(136,'Loubo',20,33,40,111,'/template/user/images/hair5.jpg','Mauris hendrerit commodo justo, sit amet tempor odio commodo id.'),(143,'Louboutin sert',20,33,40,111,'/template/user/images/hair4.jpg','Mauris hendrerit commodo justo, sit amet tempor odio commodo id.'),(4560,'sala 3d',45,60,40,112,'/template/user/images/Skin2.png','Lorem ipsum dolor sit amet, consectetur adipiscing elit.'),(4561,'lavender',25,40,50,112,'/template/user/images/Skin3.png','Lorem ipsum dolor sit amet, consectetur adipiscing elit.'),(4562,'twisted olive',32,45,30,113,'/template/user/images/JEW4.png','Lorem ipsum dolor sit amet, consectetur adipiscing elit.'),(4563,'spacious',32,45,50,113,'/template/user/images/JEW5.png','Lorem ipsum dolor sit amet, consectetur adipiscing elit.'),(4564,'Crystal Moon',45,60,60,113,'/template/user/images/JEW3.png','Lorem ipsum dolor sit amet, consectetur adipiscing elit.'),(4565,'Gel painting',14,20,40,114,'/template/user/images/nail_paint3.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit.'),(4566,'Fol KT5',40,60,50,114,'/template/user/images/nail_paint4.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit.'),(4567,'Tim-hair',58,65,40,114,'/template/user/images/nail_paint5.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `createddate` timestamp NULL DEFAULT NULL,
  `modifieddate` timestamp NULL DEFAULT NULL,
  `createdby` varchar(255) DEFAULT NULL,
  `modifiedby` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ADMIN','ADMIN',NULL,NULL,NULL,NULL),(2,'USER','USER',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `fullname` varchar(150) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `roleid` bigint(20) NOT NULL,
  `createddate` timestamp NULL DEFAULT NULL,
  `modifieddate` timestamp NULL DEFAULT NULL,
  `createdby` varchar(255) DEFAULT NULL,
  `modifiedby` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_role` (`roleid`),
  CONSTRAINT `fk_user_role` FOREIGN KEY (`roleid`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','123456','admin',1,1,NULL,NULL,NULL,NULL),(4,'tranxuanviet','123456','Xuân Việt',1,2,NULL,NULL,NULL,NULL),(5,'nakun','123456','NaKun',1,2,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-04  8:15:14
